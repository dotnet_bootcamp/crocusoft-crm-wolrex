﻿using MediatR;
using ProjectDetails.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDetails.Commands
{
    public class CreateProjectCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public List<int>? UserIds { get; set; }

        public CreateProjectCommand()
        {
            UserIds = new List<int>();
        }
    }
}
