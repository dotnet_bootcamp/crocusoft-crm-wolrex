﻿using MediatR;

namespace ProjectDetails.Commands
{
    public class UpdateProjectCommand : IRequest<bool>
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public List<int>? UserIdsToAdd { get; set; }
        public List<int>? UserIdsToDelete { get; set; }
        public UpdateProjectCommand()
        {
            UserIdsToAdd = new List<int>();
            UserIdsToDelete = new List<int>();
        }
    }
}
