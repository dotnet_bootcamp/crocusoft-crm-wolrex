﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.UserAggregate;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;

namespace ProjectDetails.Commands
{
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, bool>
    {
        private readonly IProjectRepository _projectRepo;
        private readonly IUserRepository _userRepo;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public CreateProjectCommandHandler(IProjectRepository projectRepo, IUserRepository userRepo, IMapper mapper, ApplicationDbContext context)
        {
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _mapper = mapper;
            _context = context;
        }

        public async Task<bool> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = new Project();
            project.SetDetails(request.Name);

            var createdProject = await _projectRepo.AddAsync(project);

            if (createdProject != null)
            {
                await _context.SaveChangesAsync();
            }

            foreach (var userId in request.UserIds)
            {
                var user = await _userRepo.GetAsync(userId);
                if (user != null)
                {
                    await _context.ProjectUser.AddAsync(new ProjectUser { ProjectId = project.Id, UserId = userId }, cancellationToken);
                }
            }

            await _context.SaveChangesAsync();

            return true;
        }
    }

    public class CreateProjectIdentifiedCommandHandler : IdentifiedCommandHandler<CreateProjectCommand, bool>
    {
        public CreateProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
