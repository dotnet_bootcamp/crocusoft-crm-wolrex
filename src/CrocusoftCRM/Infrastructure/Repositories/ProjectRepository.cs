﻿using Domain.AggregatesModel.ProjectAggregate;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructure;

namespace Infrastructure.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public ProjectRepository(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}
