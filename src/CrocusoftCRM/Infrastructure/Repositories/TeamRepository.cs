﻿using Domain.AggregatesModel.TeamAggregate;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructure;


namespace Infrastructure.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public TeamRepository(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}
