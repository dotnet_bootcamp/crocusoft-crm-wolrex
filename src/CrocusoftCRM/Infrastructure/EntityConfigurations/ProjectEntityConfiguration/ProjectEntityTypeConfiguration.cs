﻿using Domain.AggregatesModel.ProjectAggregate;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityConfigurations.ProjectConfiguration
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> teamConf)
        {
            teamConf.ToTable("projects");
            teamConf.HasKey(o => o.Id);
            teamConf.Property(o => o.Id).HasColumnName("id");
            teamConf.Property(o => o.Name).IsRequired().HasColumnName("name");
        }
    }
}
