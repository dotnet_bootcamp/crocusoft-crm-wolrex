﻿using Domain.AggregatesModel.UserAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.IdentityEntityConfiguration
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> userConfiguration)
        {
            userConfiguration.ToTable("users");

            userConfiguration.HasKey(o => o.Id);
            userConfiguration.Property(o => o.Id).HasColumnName("id");
            userConfiguration.Property(o => o.FirstName).HasMaxLength(100).IsRequired().HasColumnName("first_name");
            userConfiguration.Property(o => o.Email).HasMaxLength(100).IsRequired().HasColumnName("email");
            userConfiguration.Property(o => o.Phone).HasMaxLength(50).HasColumnName("phone_number");
            userConfiguration.Property(o => o.PasswordHash).IsRequired().HasColumnName("password_hash");
            userConfiguration.Property(o => o.RefreshToken).HasColumnName("refresh_token");
            userConfiguration.Property(o => o.LastName).HasColumnName("last_name");
            userConfiguration.Property(o => o.IsDeleted).HasColumnName("is_deleted");
            userConfiguration.Property(o => o.RoleId).HasColumnName("role_id");
            userConfiguration.Property(o => o.TeamId).HasColumnName("team_id");
            userConfiguration.Property(o => o.IsActive).HasColumnName("is_active");

            userConfiguration.HasOne(e => e.Role)
            .WithMany(e => e.Employees)
            .HasForeignKey(e => e.RoleId);
        }
    }
}
