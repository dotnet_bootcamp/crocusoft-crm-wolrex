﻿using Domain.AggregatesModel.RoleAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.RoleEntityConfiguration
{
    public class RoleEntityTypeConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> campaignConfiguration)
        {
            campaignConfiguration.ToTable("roles");
            campaignConfiguration.HasKey(o => o.Id);
            campaignConfiguration.Property(o => o.Id).HasColumnName("id");
            campaignConfiguration.Property(o => o.Name).IsRequired().HasColumnName("name");
            campaignConfiguration.Property(o => o.Description).HasColumnName("description");
            campaignConfiguration.Property(o => o.IsDeleted).HasColumnName("is_deleted");
        }
    }
}
