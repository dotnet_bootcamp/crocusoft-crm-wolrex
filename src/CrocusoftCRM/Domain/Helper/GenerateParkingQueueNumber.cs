﻿using System.Text;

namespace Domain.Helper
{
   public class GenerateParkingQueueNumber
    {
        public static StringBuilder GetNumber(int number)
        {
            StringBuilder generatedNumber = new StringBuilder($"{number+1}-{DateTime.UtcNow.DayOfYear}");

            return generatedNumber;
        }
    }
}
