﻿namespace NotFound.Exceptions
{
    public class NotFoundException : Exception
    {
        private readonly string _key;
        public NotFoundException(int key) => _key = key.ToString();
        public NotFoundException(string key) => _key = key;

        public NotFoundException()
        {
        }

        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}