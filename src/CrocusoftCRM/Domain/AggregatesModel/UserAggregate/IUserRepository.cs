﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.UserAggregate
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
