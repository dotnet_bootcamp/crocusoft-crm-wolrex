﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.ReportAggregate;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Domain.Exceptions;
using SharedKernel.Domain.Seedwork;
using static System.Net.WebRequestMethods;

namespace Domain.AggregatesModel.UserAggregate
{
    public class User : Entity, IAggregateRoot
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string PasswordHash { get; private set; }
        public bool IsActive { get; private set; }
        public string? RefreshToken { get; private set; }
        public bool IsDeleted { get; private set; }
        public int RoleId { get; private set; }
        public int? OTP { get; private set; }
        public DateTime? OTPExpirationDate { get; private set; }
        public Role Role { get; private set; }
        public int? TeamId { get; private set; }
        public Team? Team { get; private set; }
        public List<ProjectUser>? ProjectUser { get; set; }
        private readonly List<Report> _reports;
        public IReadOnlyCollection<Report> Reports => _reports;
        //private readonly List<Project> _projects = new List<Project>();
        //public IReadOnlyCollection<Project> Projects => _projects.AsReadOnly();

        public User(string email, string phone, string passwordHash, string firstName, string lastName) : this()
        {
            Email = email;
            Phone = phone;
            PasswordHash = passwordHash;
            FirstName = firstName;
            LastName = lastName;
            IsActive = true;
        }

        public User()
        {
            ProjectUser = new List<ProjectUser>();
            _reports = new List<Report>();
        }

        public void SetPasswordHash(string oldPasswordHash, string newPasswordHash)
        {
            if (PasswordHash != oldPasswordHash)
            {
                throw new DomainException("Invalid old password");
            }

            if (PasswordHash != newPasswordHash)
            {
                PasswordHash = newPasswordHash;
            }
        }

        public void ResetPassword(string newPasswordHash)
        {
            PasswordHash = newPasswordHash;
            OTP = null;
            OTPExpirationDate = null;
        }

        public void UpdateRefreshToken(string token)
        {
            RefreshToken = token;
        }

        public void SetDetails(string email, string phone, string firstName, string lastName)
        {
            Email = email;
            Phone = phone;
            FirstName = firstName;
            LastName = lastName;
        }

        public void SetRole(int roleId)
        {
            RoleId = roleId;
        }

        public void SetTeam(int? teamId)
        {
            TeamId = teamId;
        }

        public void Delete()
        {
            IsDeleted = true;
            IsActive = false;
        }

        public void SetActivated(bool isActive)
        {
            IsActive = isActive;
        }

        public void SetOTP(int otp)
        {
            OTP = otp;
            OTPExpirationDate = DateTime.UtcNow.AddMinutes(5);
        }
    }
}