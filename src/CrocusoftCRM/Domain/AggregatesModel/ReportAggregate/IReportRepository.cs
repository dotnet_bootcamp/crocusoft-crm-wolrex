﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ReportAggregate
{
    public interface IReportRepository : IRepository<Report>
    {
    }
}
