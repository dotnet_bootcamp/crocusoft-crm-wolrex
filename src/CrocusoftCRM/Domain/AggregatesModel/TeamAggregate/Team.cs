﻿using Domain.AggregatesModel.UserAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.TeamAggregate
{
    public class Team : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        private readonly List<User> _users;
        public IReadOnlyCollection<User> Users => _users;
        public Team()
        {
            _users = new List<User>();
        }

        public void SetDetails(string name)
        {
            Name = name;
        }
    }
}
