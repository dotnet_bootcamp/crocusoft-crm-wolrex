﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.TeamAggregate
{
    public interface ITeamRepository : IRepository<Team>
    {

    }
}
