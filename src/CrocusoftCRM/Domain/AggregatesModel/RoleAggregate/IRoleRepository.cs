﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.RoleAggregate
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}