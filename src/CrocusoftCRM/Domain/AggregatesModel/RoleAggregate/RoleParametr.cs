﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.RoleAggregate
{
    public class RoleParametr : Enumeration
    {
        public static RoleParametr SuperAdmin = new RoleParametr(1, RoleName.SuperAdmin);
        public static RoleParametr Admin = new RoleParametr(2, RoleName.Admin);
        public static RoleParametr Head = new RoleParametr(4, RoleName.Head);
        public static RoleParametr Employee = new RoleParametr(3, RoleName.Employee);

        public RoleParametr(int id, string name) : base(id, name)
        {

        }

        public RoleParametr()
        {

        }
    }
}
