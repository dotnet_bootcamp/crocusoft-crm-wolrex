﻿using Domain.AggregatesModel.UserAggregate;
using SharedKernel.Domain.Seedwork;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel.ProjectAggregate
{
    public class ProjectUser : Entity, IAggregateRoot
    {
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        [ForeignKey("ProjectId")]
        public Project Project { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}