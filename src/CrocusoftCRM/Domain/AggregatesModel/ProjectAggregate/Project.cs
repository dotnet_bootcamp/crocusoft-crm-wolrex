﻿using Domain.AggregatesModel.ReportAggregate;
using Domain.AggregatesModel.UserAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ProjectAggregate
{
    public class Project : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        private readonly List<ProjectUser>? _projectUser;
        public IReadOnlyCollection<ProjectUser> ProjectUser => _projectUser;
        private readonly List<Report> _reports;
        public IReadOnlyCollection<Report> Reports => _reports;

        public Project()
        {
            _projectUser = new List<ProjectUser>();
            _reports = new List<Report>();
        }
        public void SetDetails(string name)
        {
            Name = name;
        }

    }
}
