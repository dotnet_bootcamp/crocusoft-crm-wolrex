﻿namespace Identity.ViewModels
{
    public class AllAdminDto
    {
        public IEnumerable<UserProfileDto> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
