﻿namespace Identity.ViewModels
{
    public class AdminDto
    {
        public int Id { get; set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string Team { get; private set; }
        public string Role { get; private set; }
        private readonly List<UserProjectResponse> _userProject;
        public IReadOnlyCollection<UserProjectResponse> UserProjects => _userProject;
        public bool IsActive { get; private set; }
        public AdminDto()
        {
            _userProject = new List<UserProjectResponse>();
        }
    }
}