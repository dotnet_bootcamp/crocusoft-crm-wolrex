﻿using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.TeamAggregate;

namespace Identity.ViewModels
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string Team { get; set; }
        public string Role { get; set; }
    }
}
