﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.UserAggregate;
using Identity.ViewModels;
using Infrastructure.Constants;
using Infrastructure.Database;
using SharedKernel.Domain.Seedwork;
using System.Linq.Dynamic.Core;
using Identity.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Queries
{
    public class UserQueries : IUserQueries
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;

        public UserQueries(ApplicationDbContext context, IMapper mapper, IServiceProvider serviceProvider)
        {
            _context = context;
            _mapper = mapper;
            _serviceProvider = serviceProvider;
        }

        public async Task<User> FindAsync(int userId)
        {
            return await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(p => p.Id == userId);
        }

        public async Task<User> FindByNameAsync(string firstName)
        {
            return await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(u => u.FirstName == firstName);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<UserProfileDto> GetUserProfileAsync(int userId)
        {
            var employee = await _context.Users
            .Include(u => u.Team)
            .Include(u => u.Role)
            .FirstOrDefaultAsync(u => u.Id == userId);

            if (employee == null) return null;
            var profile = _mapper.Map<UserProfileDto>(employee);
            return profile;
        }

        public async Task<User> GetUserEntityAsync(int? userId)
        {
            var employee = await _context.Users
               .Where(u => u.Id == userId)
               .Include(u => u.Role)
               .AsNoTracking()
               .SingleOrDefaultAsync();

            if (employee == null) return null;

            return employee;
        }

        public async Task<AllAdminDto> GetAllAsync(UserRequestDto request, LoadMoreDto dto)
        {
            var userManager = _serviceProvider.GetRequiredService<IUserManager>();
            var currentUser = await userManager.GetCurrentUser();
            var entities = _context.Users.Include(p => p.Role).Include(m => m.Team)
        .Where(p =>
            (string.IsNullOrEmpty(request.FirstName) || p.FirstName.ToLower().Contains(request.FirstName.ToLower())) &&
            (string.IsNullOrEmpty(request.LastName) || p.LastName.ToLower().Contains(request.LastName.ToLower())) &&
            !p.IsDeleted)
        .AsQueryable();
            if (currentUser.Role.Name == CustomRole.Admin)
            {
                entities = _context.Users
                    .Where(x => x.Role.Name == CustomRole.Admin &&
                x.Id == currentUser.Id ||
                x.Role.Name == CustomRole.Employee)
                    .Include(r => r.Role)
                    .Include(t => t.Team)
                    .Where(x => !x.IsDeleted);
            }
            entities = (request.ProjectIds != null && request.ProjectIds
                .Any()) ? entities
                    .Where(p => p.ProjectUser.Any(pu => request.ProjectIds
                    .Contains(pu.ProjectId))) : entities;

            entities = (request.TeamIds != null && request.TeamIds.Any()) ? entities
                .Where(p => p.Team != null && request.TeamIds.Contains(p.Team.Id)) : entities;

            var count = (await entities.ToListAsync()).Count;

            if (dto.SortField != null)
            {
                entities = dto.OrderBy
                    ? entities.OrderBy($"p=>p.{dto.SortField}")
                    : entities.OrderBy($"p=>p.{dto.SortField} descending");
            }

            if (dto.Skip != null && dto.Take != null)
            {
                entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
            }

            var userModel = _mapper.Map<List<UserProfileDto>>(entities);
            var outputModel = new AllAdminDto
            {
                Data = userModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<AdminDto> GetAdminById(int id)
        {
            var entity = await _context.Users.Include(p=> p.Team).Include(p => p.Role).Include(p=> p.ProjectUser).ThenInclude(up=> up.Project).FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false && p.Role.Name != CustomRole.SuperAdmin);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<AdminDto>(entity);
        }

        public async Task<List<User>> GetUsersByTeamIdAsync(int teamId)
        {
            return await _context.Users
                .Where(e => e.TeamId == teamId)
                .ToListAsync();
        }
    }
}
