﻿using Domain.AggregatesModel.UserAggregate;
using Identity.ViewModels;
using Infrastructure.Constants;
using SharedKernel.Infrastructure.Queries;

namespace Identity.Queries
{
    public interface IUserQueries : IQuery
    {
        Task<User> FindByNameAsync(string firstName);
        Task<User> FindByEmailAsync(string email);
        Task<User> FindAsync(int userId);
        Task<UserProfileDto> GetUserProfileAsync(int userId);
        Task<User> GetUserEntityAsync(int? userId);
        public Task<AllAdminDto> GetAllAsync(UserRequestDto request, LoadMoreDto loadMore);
        public Task<AdminDto> GetAdminById(int id);
        Task<List<User>> GetUsersByTeamIdAsync(int teamId);
    }
}
