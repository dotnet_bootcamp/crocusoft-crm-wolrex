﻿using Domain.AggregatesModel.RoleAggregate;
using Identity.ViewModels;
using SharedKernel.Infrastructure.Queries;

namespace Identity.Queries
{
    public interface IRoleQueries : IQuery
    {
        Task<IEnumerable<RoleDto>> GetAllAsync();
        Task<Role> GetByName(RoleParametr role);
    }
}
