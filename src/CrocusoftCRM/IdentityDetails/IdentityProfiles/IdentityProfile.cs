﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.UserAggregate;
using Identity.ViewModels;

namespace Identity.IdentityProfiles
{
    public class IdentityProfile : Profile
    {
        public IdentityProfile()
        {
            CreateMap<User, UserProfileDto>()
            .ForMember(dest => dest.Team, opt => opt.MapFrom(src => src.Team.Name))
            .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role.Name));

            CreateMap<User, AdminDto>()
            .ForMember(dest => dest.Team, opt => opt.MapFrom(src => src.Team.Name))
            .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role.Name))
            .ForMember(dest => dest.UserProjects, opt => opt.MapFrom(src => src.ProjectUser));

            CreateMap<ProjectUser, UserProjectResponse>();
            CreateMap<Project, ProjectResponse>();

            CreateMap<Role, RoleDto>();
        }
    }
}
