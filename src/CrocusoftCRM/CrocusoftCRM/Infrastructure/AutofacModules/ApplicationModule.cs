﻿using Autofac;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Domain.AggregatesModel.UserAggregate;
using Identity;
using Identity.Auth;
using UserDetails;
using Infrastructure;
using IdentityDetails;
using test.UserDetails;
using Infrastructure.Identity;
using Infrastructure.Idempotency;
using Infrastructure.Repositories;
using TeamDetails;
using Domain.AggregatesModel.TeamAggregate;
using ProjectDetails;
using Domain.AggregatesModel.ProjectAggregate;
using ReportDetails;
using Domain.AggregatesModel.ReportAggregate;

namespace Infrastructureture.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutofacHelper.RegisterCqrsTypes<ApplicationModule>(builder);
            AutofacHelper.RegisterCqrsTypes<IdentityModule>(builder);
            AutofacHelper.RegisterCqrsTypes<UserModule>(builder);
            AutofacHelper.RegisterCqrsTypes<TeamModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ProjectModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ReportModule>(builder);

            builder.RegisterType<ClaimsManager>()
                .As<IClaimsManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserManager>()
                .As<IUserManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TeamRepository>()
                .As<ITeamRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProjectRepository>()
                .As<IProjectRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ReportRepository>()
                .As<IReportRepository>()
                .InstancePerLifetimeScope();

            // AutoMapper
            AutofacHelper.RegisterAutoMapperProfiles<ApplicationModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<IdentityModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<UserModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<TeamModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ProjectModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ReportModule>(builder);

            builder.Register(ctx =>
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    foreach (var profile in ctx.Resolve<IList<Profile>>()) cfg.AddProfile(profile);
                });
                return mapperConfiguration.CreateMapper();
            })
                .As<IMapper>()
                .SingleInstance();
        }
    }
}