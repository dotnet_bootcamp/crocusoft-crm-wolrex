﻿using System;
using System.Threading.Tasks;
using CustomAuthorizeLogic;
using Extensions;
using Identity.Queries;
using Identity.ViewModels;
using Infrastructure.Constants;
using SharedKernel.Domain.Seedwork;
using UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserDetails.Commands.Models;

namespace Controllers
{
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueries _userQueries;

        public UserController(IMediator mediator, IUserQueries userQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<RegisterUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head, CustomRole.Employee)]
        [HttpPut("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ChangePasswordCommand, bool>(command, requestId);

            return Ok();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut("resetpassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ResetPasswordCommand, bool>(command, requestId);

            return Ok();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateUserCommand, bool>(command, requestId);

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("sendopt")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ForgotPasswordCommand, bool>(command, requestId);

            return NoContent();
        }

        [AllowAnonymous]
        [HttpPatch]
        [Route("forgotpassword")]
        public async Task<IActionResult> ForgotPasswordOTP([FromBody] OTPConfirmationCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<OTPConfirmationCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPost("validate")]
        public async Task<IActionResult> ValidateToken([FromBody] CheckTokenVerifyInputCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CheckTokenVerifyInputCommand, bool>(command, requestId);

            return Ok();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteUserCommand, bool>(command, requestId);

            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPatch("active")]
        public async Task<IActionResult> Active([FromBody] ActiveUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ActiveUserCommand, bool>(command, requestId);

            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        public async Task<AllAdminDto> GetAllAsync(UserRequestDto request, LoadMoreDto loadMore)
        {
            return await _userQueries.GetAllAsync(request, loadMore);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("{id}")]
        public async Task<AdminDto> GetByIdAsync(int id)
        {
            return await _userQueries.GetAdminById(id);
        }
    }
}
