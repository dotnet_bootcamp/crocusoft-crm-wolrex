﻿using CustomAuthorizeLogic;
using Domain.AggregatesModel.TeamAggregate;
using Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SharedKernel.Domain.Seedwork;
using TeamDetails.Commands;
using TeamDetails.Queries;
using TeamDetails.ViewModels;

namespace CrocusoftCRM.Controllers
{
    [Route("team")]
    public class TeamController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ITeamQueries _teamQueries;

        public TeamController(IMediator mediator, ITeamQueries teamQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _teamQueries = teamQueries ?? throw new ArgumentNullException(nameof(teamQueries));
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("TeamsWithUsers")]
        public async Task<IEnumerable<TeamDto>> GetAllAsyncWithUsers()
        {
            return await _teamQueries.GetAllAsyncWithUsers();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("OnlyTeams")]
        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            return await _teamQueries.GetAllAsync();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("TeamById")]
        public async Task<TeamDto> GetTeamById(int id)
        {
            return await _teamQueries.GetTeamById(id);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("create")]
        public async Task<IActionResult> CreateTeam([FromBody] CreateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteTeam([FromBody] DeleteTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteTeamCommand, bool>(command, requestId);
            return NoContent();
        }
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateTeam([FromBody] UpdateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateTeamCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}
