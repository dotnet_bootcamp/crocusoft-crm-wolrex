﻿using Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectDetails.Commands;
using ProjectDetails.Queries;
using Domain.AggregatesModel.ProjectAggregate;
using ProjectDetails.ViewModels;
using CustomAuthorizeLogic;
using SharedKernel.Domain.Seedwork;
using ReportDetails.ViewModels;

namespace CrocusoftCRM.Controllers
{
    [Route("project")]
    public class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProjectQueries _projectQueries;

        public ProjectController(IMediator mediator, IProjectQueries projectQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _projectQueries = projectQueries ?? throw new ArgumentNullException(nameof(projectQueries));
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("ProjectsWithUsers")]
        public async Task<IEnumerable<ProjectDtoForGet>> GetAllAsyncWithUsers()
        {
            return await _projectQueries.GetAllAsyncWithUsers();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("OnlyProjects")]
        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await _projectQueries.GetAllAsync();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("ProjectById")]
        public async Task<ProjectDtoForGet> GetProjectById(int id)
        {
            return await _projectQueries.GetProjectById(id);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("search")]
        public async Task<IEnumerable<ProjectDtoForGet>> SearchProjects(string searchTerm)
        {
            return await _projectQueries.SearchProjects(searchTerm);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("create")]
        public async Task<IActionResult> CreateProject([FromBody] CreateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateProjectCommand, bool>(command, requestId);
            return NoContent();
        }
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateProject([FromBody] UpdateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateProjectCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet("GetProjectsWithCurrentUser")]
        public async Task<IEnumerable<ProjectDtoForCurrentUser>> GetProjectsWithCurrentUser()
        {
            return await _projectQueries.GetProjectsWithCurrentUser();
        }
    }
}
