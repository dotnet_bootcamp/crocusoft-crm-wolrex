﻿using Microsoft.AspNetCore.Http;

namespace SharedKernel.Infrastructure
{
    public static class IFormFileExtensions
    {
        public async static Task<string> SaveAsync(this IFormFile file,string path, string folder)
        {
            string baseUrl = "";
            var filePath = Path.Combine(path, folder);

            bool exists = System.IO.Directory.Exists(filePath);

            if (!exists)
                System.IO.Directory.CreateDirectory(filePath);

            var fileName = Path.Combine(Guid.NewGuid().ToString() + file.FileName);

            string resultPath = Path.Combine(filePath, fileName);

            using (var fileSteam = new FileStream(resultPath, FileMode.Create))
            {
                await file.CopyToAsync(fileSteam);
            }

            return $"{baseUrl}{folder}/{fileName}";
        }

        public static bool IsImage(this IFormFile file)
        {
            return file.ContentType.Contains("image/");
        }
    }
}
