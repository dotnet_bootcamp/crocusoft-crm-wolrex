﻿using System.Threading.Tasks;

namespace SharedKernel.Domain.Seedwork
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
        Task<T> AddAsync(T entity);
        Task<T> GetAsync(int id);
        Task<List<T>> GetByIds(List<int> ids);
        T UpdateAsync(T entity);
        bool DeleteAsync(T entity);
    }
}
