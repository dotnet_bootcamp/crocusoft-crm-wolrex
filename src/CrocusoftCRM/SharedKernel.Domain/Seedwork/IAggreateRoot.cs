﻿namespace SharedKernel.Domain.Seedwork
{
    public interface IAggregateRoot
    {
        int Id { get; set; }
    }
}
