﻿namespace SharedKernel.Domain.Seedwork
{
    public static class CustomRole
    {
        public const string Employee = "Employee";
        public const string Head = "Head";
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
    }
}
