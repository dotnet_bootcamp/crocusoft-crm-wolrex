﻿using System;
using System.Security.Cryptography;

namespace UserDetails
{
    public static class GenerateRandomNumberExtension
    {
        public static string GenerateRandomNumber()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
