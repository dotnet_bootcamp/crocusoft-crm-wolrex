﻿using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using SharedKernel.Infrastructure;

namespace UserDetails.Commands
{
    public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserManager _userManager;

        public ResetPasswordCommandHandler(IUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<bool> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(request.UserId);

            if (user == null)
            {
                throw new Exception("User not found");
            }

            var newPasswordHash = PasswordHasher.HashPassword(request.NewPassword);
            user.ResetPassword(newPasswordHash);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class ResetPasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ResetPasswordCommand, bool>
    {
        public ResetPasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
