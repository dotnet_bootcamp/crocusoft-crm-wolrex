﻿using FluentValidation;

namespace UserDetails.Commands
{
    public class ActiveUserCommandValidator : AbstractValidator<ActiveUserCommand>
    {
        public ActiveUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.IsActive).NotNull();
        }
    }
}
