﻿using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructure;
using UserDetails.Commands.Models;

namespace UserDetails.Commands
{
    public class OTPConfirmationHandler : IRequestHandler<OTPConfirmationCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly ApplicationDbContext _context;

        public OTPConfirmationHandler(IUserRepository userRepository, ApplicationDbContext context)
        {
            _userRepository = userRepository;
            _context = context;
        }

        public async Task<bool> Handle(OTPConfirmationCommand request, CancellationToken cancellationToken)
        {
            User? user = await _context
                .Users
                .FirstOrDefaultAsync(u => u.OTP == request.OTP, cancellationToken: cancellationToken)
                ?? throw new UnauthorizedAccessException("Invalid OTP Code.");

            if (user.OTPExpirationDate > DateTime.UtcNow.AddMinutes(5))
            {
                throw new DomainException("OTP code expired");
            }

            if (request.Password != request.ConfirmPassword)
            {
                throw new DomainException("New password and confirmation password do not match");
            }

            string? newPasswordHash = PasswordHasher.HashPassword(request.Password);
            user.ResetPassword(newPasswordHash);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class OTPConfirmationIdentifiedCommandHandler : IdentifiedCommandHandler<OTPConfirmationCommand, bool>
    {
        public OTPConfirmationIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}