﻿using MediatR;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure;
using SharedKernel.Infrastructure.Helper;

namespace UserDetails.Commands
{
    public class ForgotPasswordCommandHandler : IRequestHandler<ForgotPasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userService;
        private readonly ISendEmailAsync _sendEmailAsync;

        public ForgotPasswordCommandHandler(IUserRepository userRepository, IUserQueries userQueries, ISendEmailAsync sendEmailAsync)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userService = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _sendEmailAsync = sendEmailAsync;
        }

        public async Task<bool> Handle(ForgotPasswordCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userService.FindByEmailAsync(request.Email) ?? throw new DomainException("Provided email is not associated with any Crocusoft account");

            _ = int.TryParse(GenerateOTP(), out int result);
            user.SetOTP(result);

            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            string subject = "Forgot Password Request";
            string body = $@"<html><body><p>Dear {user.FirstName}, </p><p>You have requested password change for CrocusoftCRM.<br></br> Your OTP Code: <strong>{result}</strong>" + "<br></br> This code will expire in 5 minutes." + "<strong></p> DO NOT SHARE IT WITH ANYONE, NOT EVEN ADMINISTRATIVE ACCOUNTS! <br></br> If you are facing difficulties or didn't request a password change, alert our administrators.</p><br><p> Kind Regards,</p><p> Crocusoft. </p> ";
            await _sendEmailAsync.SendEmailUserAsync(user.Email, subject, body, null);

            return true;
        }

        private static string GenerateOTP()
        {
            Random random = new();
            const string chars = "0123456789";

            return new string(Enumerable.Repeat(chars, 6)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    public class ForgotPasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ForgotPasswordCommand, bool>
    {
        public ForgotPasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
