﻿using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Identity.Queries;
using SharedKernel.Infrastructure;
using UserDetails.Commands.Models;
using MediatR;
using System.Security.Authentication;

namespace UserDetails.Commands
{
    public class GetAuthorizationTokenCommandHandler : IRequestHandler<GetAuthorizationTokenCommand, JwtTokenDTO>
    {
        private readonly IUserManager _userManager;
        private readonly IUserQueries _userQueries;
        private readonly IUserRepository _userRepository;

        public GetAuthorizationTokenCommandHandler(IUserManager userManager, IUserQueries userQueries, IUserRepository userRepository )
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<JwtTokenDTO> Handle(GetAuthorizationTokenCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email.ToLower();
            var employee = await _userQueries.FindByEmailAsync(email);
            if (employee == null || employee.PasswordHash != PasswordHasher.HashPassword(request.Password) || employee.IsDeleted || employee.IsActive == false)
                throw new AuthenticationException("Invalid credentials.");

            (string token, DateTime expiresAt) = _userManager.GenerateJwtToken(employee);

            var refreshTokenValue = "";
            if(employee.RefreshToken != null)
            {
                var splitToken = employee.RefreshToken.Split("_");

                if (Convert.ToDateTime(splitToken[2]) < DateTime.Now)
                {
                    var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                    var refreshToken = $"{randomNumber}_{employee.Id}_{DateTime.UtcNow.AddDays(30)}";
                    refreshTokenValue = refreshToken;
                    employee.UpdateRefreshToken(refreshToken);
                }
                else
                {
                    refreshTokenValue = employee.RefreshToken;
                }
            }
            else
            {
                var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                var refreshToken = $"{randomNumber}_{employee.Id}_{DateTime.UtcNow.AddDays(30)}";
                refreshTokenValue = refreshToken;
                employee.UpdateRefreshToken(refreshToken);
            }

            _userRepository.UpdateAsync(employee);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return new JwtTokenDTO
            {
                Token = token,
                RefreshToken = refreshTokenValue,
                ExpiresAt = expiresAt
            };
        }
    }
}
