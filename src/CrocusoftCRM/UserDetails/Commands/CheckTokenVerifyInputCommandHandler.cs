﻿using Identity.Auth;
using Identity.Queries;
using Infrastructure;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using UserDetails.Commands.Models;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace UserDetails.Commands
{
    public class CheckTokenVerifyInputCommandHandler : IRequestHandler<CheckTokenVerifyInputCommand, bool>
    {
        private readonly IUserManager _employeeManager;
        private readonly IUserQueries _employeeQueries;
        private readonly CrocusoftCRMSettings _saffarmaSettings;


        public CheckTokenVerifyInputCommandHandler(IUserManager userManager, IUserQueries userQueries, IOptions<CrocusoftCRMSettings> saffarmaSettings)
        {
            _employeeManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _employeeQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _saffarmaSettings = saffarmaSettings.Value ?? throw new ArgumentNullException(nameof(saffarmaSettings));
        }

        public async Task<bool> Handle(CheckTokenVerifyInputCommand request, CancellationToken cancellationToken)
        {
            var userName = request.UserName.ToLower();
            var user = await _employeeQueries.FindByNameAsync(userName);
            var userId = user.Id.ToString();  //because of validateTokenUserId , it accepts only String value

            if (user == null)
                throw new AuthenticationException("Invalid credentials.");

            var validateTokenUserId = TokenManager.ValidateToken(_saffarmaSettings, request.Token);

            if (validateTokenUserId == null)
            {
                throw new AuthenticationException("Token is null");
            }


            if (!userId.Equals(validateTokenUserId))
            {
                throw new AuthenticationException("Token is invalid for this user");
            }

            return true;
        }

        public class CheckTokenInputCommandHandler : IdentifiedCommandHandler<CheckTokenVerifyInputCommand, bool>
        {
            public CheckTokenInputCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
