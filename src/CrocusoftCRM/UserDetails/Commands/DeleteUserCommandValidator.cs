﻿using FluentValidation;

namespace UserDetails.Commands
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
        }
    }
}
