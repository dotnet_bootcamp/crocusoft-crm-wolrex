﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetails.Commands
{
    public class ResetPasswordCommand : IRequest<bool>
    {
        public int UserId { get; set; }
        public string NewPassword { get; set; }
    }
}
