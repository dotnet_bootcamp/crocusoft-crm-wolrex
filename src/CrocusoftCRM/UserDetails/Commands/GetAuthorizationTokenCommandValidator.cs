﻿using FluentValidation;

namespace UserDetails.Commands
{
    class GetAuthorizationTokenCommandValidator : AbstractValidator<GetAuthorizationTokenCommand>
    {
        public GetAuthorizationTokenCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.Password).NotNull();
        }
    }
}
