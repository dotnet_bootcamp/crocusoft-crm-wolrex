﻿using FluentValidation;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace TeamDetails.Commands
{
    public class UpdateTeamCommandValidator : AbstractValidator<UpdateTeamCommand>
    {
        public UpdateTeamCommandValidator() : base()
        {
            RuleFor(command => command.Name).NotNull();
        }
    }
}
