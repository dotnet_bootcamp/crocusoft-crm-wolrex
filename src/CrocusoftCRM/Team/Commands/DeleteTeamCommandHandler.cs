﻿using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using Infrastructure.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamDetails.Commands
{
    public class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepo;
        private readonly IUserRepository _userRepo;
        private readonly IUserQueries _userQuery;
        private readonly ApplicationDbContext _context;
        public DeleteTeamCommandHandler(ITeamRepository teamRepo, IUserRepository userRepo, IUserQueries userQuery, ApplicationDbContext context)
        {
            _teamRepo = teamRepo;
            _userRepo = userRepo;
            _userQuery = userQuery;
            _context = context;
        }


        public async Task<bool> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            var teamId = request.Id;

            var existingTeam = await _teamRepo.GetAsync(teamId);

            if (existingTeam == null)
            {
                throw new DomainException($"Team with id {teamId} not found.");
            }

            var usersInTeam = await _userQuery.GetUsersByTeamIdAsync(teamId);

            if (usersInTeam.Any())
            {
                throw new DomainException($"Cannot delete the team as it still has associated users.");
            }

            _teamRepo.DeleteAsync(existingTeam);
            await _teamRepo.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return true;
        }
    }

    public class DeleteIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteTeamCommand, bool>
    {
        public DeleteIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
