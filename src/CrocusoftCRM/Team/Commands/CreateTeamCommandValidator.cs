﻿using FluentValidation;

namespace TeamDetails.Commands
{
    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator() : base()
        {
            RuleFor(command => command.Name).NotNull();
        }
    }
}
