﻿using Domain.AggregatesModel.TeamAggregate;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using Infrastructure.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace TeamDetails.Commands
{
    public class CreateTeamCommandHandler : IRequestHandler<CreateTeamCommand, bool>
    {
        private readonly ITeamRepository _repo;
        private readonly ApplicationDbContext _context;

        public CreateTeamCommandHandler(ITeamRepository repo, ApplicationDbContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task<bool> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var teamNameToLower = request.Name.ToLower();

            var existingTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Name.ToLower() == teamNameToLower);

            if (existingTeam != null)
            {
                throw new ApplicationException($"The team '{request.Name}' already exists.");
            }

            var newTeam = new Team();
            newTeam.SetDetails(request.Name);

            await _repo.AddAsync(newTeam);
            await _repo.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return true;
        }
    }

    public class CreateTeamIdentifiedCommandHandler : IdentifiedCommandHandler<CreateTeamCommand, bool>
    {
        public CreateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
