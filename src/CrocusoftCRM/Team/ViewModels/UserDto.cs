﻿namespace TeamDetails.ViewModels
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
