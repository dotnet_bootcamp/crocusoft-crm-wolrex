﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.UserAggregate;
using TeamDetails.ViewModels;


namespace TeamDetails.TeamProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>()
             .ForMember(dest => dest.Users, opt => opt.MapFrom(src => src.Users));

            CreateMap<User, UserDto>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName));

        }
    }
}
