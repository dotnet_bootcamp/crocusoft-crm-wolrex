﻿using Domain.AggregatesModel.TeamAggregate;
using SharedKernel.Infrastructure.Queries;
using TeamDetails.ViewModels;

namespace TeamDetails.Queries
{
    public interface ITeamQueries : IQuery
    {
        Task<IEnumerable<TeamDto>> GetAllAsyncWithUsers();
        Task<IEnumerable<Team>> GetAllAsync();
        Task<TeamDto> GetTeamById(int id);
    }
}
