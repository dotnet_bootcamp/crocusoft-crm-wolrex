﻿using Infrastructure.Constants;
using System;
using System.Collections.Generic;
namespace ReportDetails.DTO
{
    public class GetSelfDailyReportsDTO
    {
        public List<int>? ProjectIds { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public LoadMoreDto? LoadMore { get; set; }
    }
}
