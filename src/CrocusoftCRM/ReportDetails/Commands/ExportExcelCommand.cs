﻿using MediatR;
using ReportDetails.DTO;
using ReportDetails.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportDetails.Commands
{
    public class ExportExcelCommand : IRequest<FileResponse>
    {
        public DailyReportFilterDTO ReportFilter { get; set; }
    }

}
