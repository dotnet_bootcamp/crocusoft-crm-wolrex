﻿using MediatR;

namespace ReportDetails.Commands
{
    public class CreateReportCommand : IRequest<bool>
    {
        public string Text { get; set; }
        public int ProjectId { get; set; }
        public CreateReportCommand(string text, int projectId)
        {
            Text = text;
            ProjectId = projectId;
        }
    }
}
