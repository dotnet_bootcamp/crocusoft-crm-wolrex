﻿using Domain.Exceptions;
using MediatR;
using ReportDetails.Queries;
using ReportDetails.Response;
using ReportDetails.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportDetails.Commands
{
    public class ExportExcelCommandHandler : IRequestHandler<ExportExcelCommand, FileResponse>
    {
        private readonly IReportQueries _query;
        public ExportExcelCommandHandler(IReportQueries query)
        {
            _query = query;
        }

        public async Task<FileResponse> Handle(ExportExcelCommand request, CancellationToken cancellationToken)
        {
            var reports = await _query.GetDailyReportFilter(request.ReportFilter);

            if (reports.TotalCount == 0)
                throw new DomainException("No reports found. Export failed.");

            MemoryStream memoryStream = new(ExportToExcelService.ExportToExcel(reports.Data.ToList()));

            return new FileResponse
            {
                FileStream = memoryStream,
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                FileName = $"{DateTime.UtcNow} Report"
            };
        }
    }

}
