﻿
using MediatR;

namespace ReportDetails.Commands
{
    public class UpdateReportCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Text { get; set; }
    }
}
