﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.UserAggregate;

namespace ReportDetails.ViewModels
{
    public class ReportDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string User { get; set; }
        public string Project { get; set; }
        public DateTime RecordDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime UpdateLimitDate { get; set; }
    }
}
