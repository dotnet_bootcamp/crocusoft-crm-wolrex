﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.ReportAggregate;
using Domain.AggregatesModel.UserAggregate;
using ReportDetails.Response;
using ReportDetails.ViewModels;

namespace ReportDetails.ReportProfiles
{
    public class ReportProfile : Profile
    {
        public ReportProfile()
        {
            CreateMap<Report, ReportDto>()
            .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Text))
            .ForMember(dest => dest.User, opt => opt.MapFrom(src => $"{src.created_by.FirstName} {src.created_by.LastName}"))
            .ForMember(dest => dest.Project, opt => opt.MapFrom(src => src.Project.Name))
            .ForMember(dest => dest.RecordDate, opt => opt.MapFrom(src => src.record_date))
            .ForMember(dest => dest.LastUpdateDate, opt => opt.MapFrom(src => src.last_update_date))
            .ForMember(dest => dest.UpdateLimitDate, opt => opt.MapFrom(src => src.UpdateLimit));

            CreateMap<Report, DailyReportResponse>();
            CreateMap<User, DailyReportUserResponse>()
                .ForMember(dest => dest.Fullname, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"));
            CreateMap<Project, DailyReportProjectResponse>();
        }
    }
}
