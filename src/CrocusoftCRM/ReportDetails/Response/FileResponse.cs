﻿namespace ReportDetails.Response
{
    public class FileResponse
    {
        public Stream FileStream { get; set; }
        public string ContentType { get; set; } 
        public string FileName { get; set; } 
    }
}