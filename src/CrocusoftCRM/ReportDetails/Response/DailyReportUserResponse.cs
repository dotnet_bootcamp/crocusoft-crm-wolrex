﻿namespace ReportDetails.Response
{
    public class DailyReportUserResponse
    {
        public string Fullname { get; set; }
        public string Email { get; set; }
    }
}